﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DocumentScanEngine_API.GlobalData
{
    public class GUIDNo
    {
        public static string getUniqueCode()
        {
            string uniqueNo = "";
            try
            {
                uniqueNo = Guid.NewGuid().ToString();
                return uniqueNo;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}