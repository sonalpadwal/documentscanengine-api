﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace DocumentScanEngine_API.GlobalData
{
    public class LogError
    {
        public static void TraceException(Exception ex,string extraDescription)
        {
            string filePath = @"C:\inetpub\wwwroot\ExpenseDoc Storage\ErrorLogged.txt";

            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                writer.WriteLine("-----------------------Start------------------------------------------------");
                writer.WriteLine("Date : " + DateTime.Now.ToString());
                writer.WriteLine();

                while (ex != null)
                {
                    writer.WriteLine("Extra Description: "+ extraDescription);
                    writer.WriteLine(ex.GetType().FullName);
                    writer.WriteLine("Message : " + ex.Message);
                    writer.WriteLine("StackTrace : " + ex.StackTrace);

                    ex = ex.InnerException;
                }
                writer.WriteLine("----------------------------End---------------------------------");
                writer.WriteLine();
            }

        }
    }
}