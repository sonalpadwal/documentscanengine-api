﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DocumentScanEngine_API.Models
{
    public class DocumentDetails
    {
        public DocumentMaster documentMaster { get; set; }
        public List<DocumentTable> documentTables { get; set; }
    }
}