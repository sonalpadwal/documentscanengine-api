﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DocumentScanEngine_API.Models
{
    public class DocumentJSON
    {
        public string FileBytes { get; set; }
        public string FileName { get; set; }
    }
}