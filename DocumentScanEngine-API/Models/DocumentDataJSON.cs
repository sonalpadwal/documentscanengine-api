﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DocumentScanEngine_API.Models
{
    public class DocumentDataJSON
    {
        public int count { get; set; }
        public DocumentDetails documentDetails { get; set; }
    }
}