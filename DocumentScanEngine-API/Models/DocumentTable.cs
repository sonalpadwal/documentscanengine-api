﻿using DocumentScanEngine_API.GlobalData;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace DocumentScanEngine_API.Models
{
    public class DocumentTable
    {
        public string Description { get; set; }
        public string Quantity { get; set; }

        private string tempDate;
        public string Date
        {
            get { return tempDate; }
            set
            {
                if (value != "")
                {
                    try
                    {
                        string[] formats = { "M/d/yyyy", "d/M/yyyy", "M-d-yyyy", "d-M-yyyy", "d-MMM-yy", "d-MMMM-yyyy", };
                        DateTime date;
                        if (DateTime.TryParseExact(value.ToString(), formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                        {
                            tempDate = date.ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            tempDate = DateTime.UtcNow.ToString("dd/MM/yyyy");
                        }
                        //string strDate = value;
                        //DateTime datTemp = DateTime.ParseExact(strDate, "dd/MM/yyyy", CultureInfo.InvariantCulture); ;
                        //tempDate = datTemp.ToShortDateString();
                    }
                    catch (Exception ex)
                    {
                        LogError.TraceException(ex, "Table Prop - " + value);
                        tempDate = DateTime.UtcNow.ToString("dd/MM/yyyy");
                    }
                }
                else
                {
                    tempDate = DateTime.UtcNow.ToString("dd/MM/yyyy");
                }
            }
        }

        private string tempUnitPrice;

        public string UnitPrice
        {
            get { return tempUnitPrice; }
            set { tempUnitPrice = value; }
        }

        private string tempAmount;

        public string Amount
        {
            get { return tempAmount; }
            set
            {
                if(value!="")
                {
                    try
                    {
                        tempAmount = value;
                    }
                    catch (Exception ex)
                    {
                        LogError.TraceException(ex, "Table Prop - " + value);
                        tempAmount = "0.00";
                    }
                }
                else
                {
                    tempAmount = "0.00";
                }
            }
        }

        private string tempVAT;

        public string VAT
        {
            get { return tempVAT; }
            set
            {
                if (value != "")
                {
                    try
                    {
                        tempVAT = value;
                    }
                    catch (Exception ex)
                    {
                        LogError.TraceException(ex, "Table Prop - " + value);
                        tempVAT = "0.00";
                    }
                }
                else
                {
                    tempVAT = "0.00";
                }
            }
        }
    }
}