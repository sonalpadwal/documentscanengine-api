﻿using DocumentScanEngine_API.GlobalData;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;

namespace DocumentScanEngine_API.Models
{
    public class DocumentMaster
    {
        public static string globalDownloadURL = ConfigurationManager.AppSettings["DownloadURL"];
        public string DocumentUniuqId { get; set; }
        public string DocumentNo { get; set; }
        public string DocumentCurrency { get; set; }

        private string tempDate;
        public string DocumentDate
        {
            get { return tempDate; }
            set
            {
                if (value != "")
                {
                    try
                    {
                        string[] formats = { "M/d/yyyy", "d/M/yyyy", "M-d-yyyy", "d-M-yyyy", "d-MMM-yy", "d-MMMM-yyyy", "dd MMMM yyyy" };
                        DateTime date;
                        if (DateTime.TryParseExact(value.ToString(), formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                        {
                            tempDate = date.ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            tempDate = DateTime.UtcNow.ToString("dd/MM/yyyy");
                        }
                        //string strDate = value;
                        //DateTime datTemp = DateTime.ParseExact(strDate, "dd/MM/yyyy", CultureInfo.InvariantCulture); ;
                        //tempDate = datTemp.ToShortDateString();
                    }
                    catch (Exception ex)
                    {
                        LogError.TraceException(ex, "Master Table Prop - " + value);
                        tempDate = DateTime.Now.ToString("dd/MM/yyyy");
                    }
                }
                else
                {
                    tempDate = DateTime.Now.ToString("dd/MM/yyyy");
                }
            }
        }

        private decimal tempAmount;

        public decimal Amount
        {
            get { return tempAmount; ; }
            set { tempAmount = value; }
        }

        private decimal tempVAT;

        public decimal VAT
        {
            get { return tempVAT; }
            set { tempVAT = value; }
        }

        private decimal tempNetAmount = 0;
        public decimal NetAmount
        {
            get { return tempNetAmount; }
            set { tempNetAmount = Amount + VAT; }
        }
        public string VATNo { get; set; }

        private string finalDownloadURl = "";
        public string DownloadURL
        {
            get { return finalDownloadURl; }
            set
            {
                finalDownloadURl = globalDownloadURL + value + ".pdf";
            }
        }


        //Future implementations
        public string CompanyName { get; set; }
        public string CompanyCategory { get; set; }
        public string CompanyRegisteredName { get; set; }
        public string CompanyAddress { get; set; }
        public string DocumentFormat { get; set; }
        public string PaymentType { get; set; }
    }
}