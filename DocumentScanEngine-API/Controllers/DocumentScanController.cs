﻿using DocumentScanEngine_API.GlobalData;
using DocumentScanEngine_API.Models;
using DocumentScanEngine_API.ProcessData;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace DocumentScanEngine_API.Controllers
{

    public class DocumentScanController : ApiController
    {
        ScanningProcess objDataProcess = new ScanningProcess();

        [HttpGet]
        public HttpResponseMessage Get()
        {
            //string fielPath = @"C:\Pravins Workspace\Delete Folder\b5cb9291-de33-4631-905f-c39f395120fe.xml";
            //ScanningProcess scan = new ScanningProcess();
            //scan.ReadDocumentData("sadasdsadasd", fielPath, "Date Test");
            return Request.CreateResponse(HttpStatusCode.OK,"DocumentScan Engine API called successfully.");
        }

        [HttpPost]
        public HttpResponseMessage Post(HttpRequestMessage requestFile)
        {
            try
            {
                JObject jsonData = objDataProcess.DocumentProcessing(requestFile);
                return Request.CreateResponse(HttpStatusCode.OK, jsonData);
            }
            catch (Exception ex)
            {
                LogError.TraceException(ex, "-");
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }



    }
}
