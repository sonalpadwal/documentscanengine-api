﻿using DocumentScanEngine_API.GlobalData;
using DocumentScanEngine_API.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml.Linq;

namespace DocumentScanEngine_API.ProcessData
{
    public class ScanningProcess
    {
        public JObject DocumentProcessing(HttpRequestMessage requestFile)
        {
            string strUniqueNo = GUIDNo.getUniqueCode();
            string filePath = "";
            string body = "";
            string fileContent = "";
            string fileName = "";
            try
            {
                body = requestFile.Content.ReadAsStringAsync().Result;
                //body = body.Replace("[", "");
                //body = body.Replace("]", "");

                //seperate file data and file name
                //DocumentJSON resultTest = JsonConvert.DeserializeObject<DocumentJSON>(body);
                //fileName = resultTest.FileName;
                fileContent = body;


                //get file data only
                //fileContent = fileContent.Replace("data:application/pdf;base64,", "");

                //Export physical document into local directory
                if (!Directory.Exists(@"C:\inetpub\wwwroot\ExpenseDoc Storage\"))
                {
                    Directory.CreateDirectory(@"C:\inetpub\wwwroot\ExpenseDoc Storage\");
                }
                filePath = @"C:\inetpub\wwwroot\ExpenseDoc Storage\" + strUniqueNo + ".pdf";
                byte[] sPDFDecoded = Convert.FromBase64String(fileContent);
                File.WriteAllBytes(filePath, sPDFDecoded);

                //Upload file to import folder
                UploadFile(strUniqueNo, filePath);

                //Download XML file from export folder - Scan Engine Server sonal commented this for right now
                while (true)
                {
                    if (DownloadDataFile(strUniqueNo, filePath))
                    {
                        break;
                    }
                }

                //Create JSON Data 
                DocumentDataJSON finalJSONData = ReadDocumentData(strUniqueNo, filePath, fileName);
                if (finalJSONData == null)
                {
                    finalJSONData = new DocumentDataJSON();
                    finalJSONData.count = 0;
                    finalJSONData.documentDetails = null;
                }
                else
                {
                    finalJSONData.count = 1;
                }

                string jsonResult = JsonConvert.SerializeObject(finalJSONData);
                JObject jsonData = JObject.Parse(jsonResult);
                return jsonData;
            }
            catch (Exception ex)
            {
                LogError.TraceException(ex, "-");
                DocumentDataJSON finalJSONData = new DocumentDataJSON();
                finalJSONData.count = 0;
                finalJSONData.documentDetails = null;
                string jsonResult = JsonConvert.SerializeObject(finalJSONData);
                JObject jsonData = JObject.Parse(jsonResult);
                return jsonData;
            }
        }

        //Document Scanning and reading values
        public DocumentDataJSON ReadDocumentData(string fileUniqueId, string filePath, string fileName)
        {
            DocumentDataJSON documentDataJSON = new DocumentDataJSON();

            decimal taxPercentage = 0;
            string docName = "";
            string invoiceType = "";
            try
            {
                System.Xml.XmlTextReader reader = new System.Xml.XmlTextReader(filePath);
                DataSet newTable = new DataSet();
                newTable.ReadXml(reader);
                DataTable dtExtractedValues = newTable.Tables[0];
                DataTable dtTable = new DataTable();
                try
                {
                    if (newTable.Tables[1] != null)
                    {
                        dtTable = newTable.Tables[1];

                    }
                }
                catch(Exception)
                {

                }
                string tableName = dtExtractedValues.TableName.Replace("_", ""); 
                string TableNameNew = dtTable.TableName.Replace("_", "");
                invoiceType = tableName;
                reader.Dispose();               
                 


                if(TableNameNew=="Table1")
                {
                    dtTable.Columns.Add("_InvoiceDate");
                    dtTable.Columns.Add("_Ourref");
                    dtTable.Columns.Add("_Mins");
                }
                else if(TableNameNew=="Table")
                {
                    dtTable.Columns.Add("_InvoiceDate");
                    dtTable.Columns.Add("_Ourref");
                    dtTable.Columns.Add("_Mins");
                    dtTable.Columns.Add("_Price");
                    dtTable.Columns.Add("_Total");
                    dtTable.Columns.Add("_Vat");
                }
                    
                dtExtractedValues.Columns.Add("_DateNew");
                dtExtractedValues.Columns.Add("_Time");
                dtExtractedValues.Columns.Add("_Service");
                dtExtractedValues.Columns.Add("_Ourref");
                dtExtractedValues.Columns.Add("_YourReference");
                dtExtractedValues.Columns.Add("_Price");
                dtExtractedValues.Columns.Add("_Mins");
                dtExtractedValues.Columns.Add("_Wait");
                dtExtractedValues.Columns.Add("_Misc");
                dtExtractedValues.Columns.Add("_Total");
                dtExtractedValues.Columns.Add("_Vat");

                decimal invoiceAmount = 0;
                decimal vatAmount = 0;
                bool IsAddissionLee = false;
                string CurSysmbol = "";

                //Collecting Invoice Amount
                if (dtExtractedValues.Columns.Contains("_InvoiceAmount") && dtExtractedValues.Rows[0]["_InvoiceAmount"].ToString() != "")
                {
                    string invAmt = dtExtractedValues.Rows[0]["_InvoiceAmount"].ToString().Trim();

                    int dotCount = invAmt.Count(x => x == '.');
                    if (dotCount > 1)
                    {
                        int indexOfDot = invAmt.LastIndexOf(".");
                        invAmt = invAmt.Remove(indexOfDot, 1);
                    }

                    try
                    {
                        if (invAmt.Contains("£"))
                        {
                            invAmt = invAmt.Replace("£", "");
                            CurSysmbol = "£";
                        }
                        else if (invAmt.Contains("$"))
                        {
                            invAmt = invAmt.Replace("$", "");
                            CurSysmbol = "$";
                        }
                        else if (invAmt.Contains("€"))
                        {
                            invAmt = invAmt.Replace("€", "");
                            CurSysmbol = "€";
                        }

                        if (invAmt.Contains("GBP"))
                        {
                            invAmt = invAmt.Replace("GBP", "");
                            CurSysmbol = "GBP";
                        }
                        if (invAmt.Contains("USD"))
                        {
                            invAmt = invAmt.Replace("USD", "");
                            CurSysmbol = "USD";
                        }
                        if (invAmt.Contains("EUR"))
                        {
                            invAmt = invAmt.Replace("EUR", "");
                            CurSysmbol = "EUR";
                        }

                        if (invAmt.Contains(","))
                        {
                            invAmt = invAmt.Replace(",", "");
                        }


                        invoiceAmount = Convert.ToDecimal(invAmt);
                    }
                    catch (Exception)
                    {
                        invAmt = "0";
                        invoiceAmount = 0;
                    }
                }

                if (dtExtractedValues.Columns.Contains("_InvoiceTotal") && dtExtractedValues.Rows[0]["_InvoiceTotal"].ToString() != "")
                {
                    string invAmt = dtExtractedValues.Rows[0]["_InvoiceTotal"].ToString().Trim();

                    int dotCount = invAmt.Count(x => x == '.');
                    if (dotCount > 1)
                    {
                        int indexOfDot = invAmt.LastIndexOf(".");
                        invAmt = invAmt.Remove(indexOfDot, 1);
                    }

                    try
                    {
                        if (invAmt.Contains("£"))
                        {
                            invAmt = invAmt.Replace("£", "");
                            CurSysmbol = "£";
                        }
                        else if (invAmt.Contains("$"))
                        {
                            invAmt = invAmt.Replace("$", "");
                            CurSysmbol = "$";
                        }
                        else if (invAmt.Contains("€"))
                        {
                            invAmt = invAmt.Replace("€", "");
                            CurSysmbol = "€";
                        }

                        if (invAmt.Contains("GBP"))
                        {
                            invAmt = invAmt.Replace("GBP", "");
                            CurSysmbol = "GBP";
                        }
                        if (invAmt.Contains("USD"))
                        {
                            invAmt = invAmt.Replace("USD", "");
                            CurSysmbol = "USD";
                        }
                        if (invAmt.Contains("EUR"))
                        {
                            invAmt = invAmt.Replace("EUR", "");
                            CurSysmbol = "EUR";
                        }

                        if (invAmt.Contains(","))
                        {
                            invAmt = invAmt.Replace(",", "");
                        }


                        invoiceAmount = Convert.ToDecimal(invAmt);
                    }
                    catch (Exception)
                    {
                        invAmt = "0";
                        invoiceAmount = 0;
                    }
                }
                //Collecting VAT Percentage
                if (dtExtractedValues.Columns.Contains("_VATPercentage") && dtExtractedValues.Rows[0]["_VATPercentage"].ToString() != "")
                {
                    string tempVATPer = dtExtractedValues.Rows[0]["_VATPercentage"].ToString();
                    if (tempVATPer.Contains("20"))
                    {
                        taxPercentage = 20;
                    }
                }
                //Collecting VAT Amount
                if (dtExtractedValues.Columns.Contains("_VATAmount") && dtExtractedValues.Rows[0]["_VATAmount"].ToString() != "")
                {
                    string vatAmt = dtExtractedValues.Rows[0]["_VATAmount"].ToString().Trim();
                    if (vatAmt.Contains("£"))
                    {
                        vatAmt = vatAmt.Replace("£", "");
                    }
                    else if (vatAmt.Contains("$"))
                    {
                        vatAmt = vatAmt.Replace("$", "");
                    }
                    else if (vatAmt.Contains("€"))
                    {
                        vatAmt = vatAmt.Replace("€", "");
                    }

                    if (vatAmt.Contains("GBP"))
                    {
                        vatAmt = vatAmt.Replace("GBP", "");
                    }
                    if (vatAmt.Contains("USD"))
                    {
                        vatAmt = vatAmt.Replace("USD", "");
                    }
                    if (vatAmt.Contains("EUR"))
                    {
                        vatAmt = vatAmt.Replace("EUR", "");
                    }

                    if (vatAmt.Contains(","))
                    {
                        vatAmt = vatAmt.Replace(",", "");
                    }

                    try
                    {
                        vatAmount = Convert.ToDecimal(vatAmt);

                        if (vatAmount > invoiceAmount)
                        {
                            vatAmount = vatAmount - invoiceAmount;
                        }
                        else if (vatAmount == invoiceAmount)
                        {
                            vatAmount = 0;
                        }
                    }
                    catch (Exception)
                    {
                        vatAmt = "0";
                        vatAmount = 0;
                    }

                    if (vatAmount > 0 && taxPercentage == 0 && invoiceAmount!=0)
                    {
                        decimal tempPer = vatAmount * 100;
                        taxPercentage = tempPer / invoiceAmount;
                        taxPercentage = Math.Round(taxPercentage, 2);
                    }
                }

                if (dtExtractedValues.Columns.Contains("_VatTotal") && dtExtractedValues.Rows[0]["_VatTotal"].ToString() != "")
                {
                    string vatAmt = dtExtractedValues.Rows[0]["_VatTotal"].ToString().Trim();
                    if (vatAmt.Contains("£"))
                    {
                        vatAmt = vatAmt.Replace("£", "");
                    }
                    else if (vatAmt.Contains("$"))
                    {
                        vatAmt = vatAmt.Replace("$", "");
                    }
                    else if (vatAmt.Contains("€"))
                    {
                        vatAmt = vatAmt.Replace("€", "");
                    }

                    if (vatAmt.Contains("GBP"))
                    {
                        vatAmt = vatAmt.Replace("GBP", "");
                    }
                    if (vatAmt.Contains("USD"))
                    {
                        vatAmt = vatAmt.Replace("USD", "");
                    }
                    if (vatAmt.Contains("EUR"))
                    {
                        vatAmt = vatAmt.Replace("EUR", "");
                    }

                    if (vatAmt.Contains(","))
                    {
                        vatAmt = vatAmt.Replace(",", "");
                    }

                    try
                    {
                        vatAmount = Convert.ToDecimal(vatAmt);

                        if (vatAmount > invoiceAmount)
                        {
                            vatAmount = vatAmount - invoiceAmount;
                        }
                        else if (vatAmount == invoiceAmount)
                        {
                            vatAmount = 0;
                        }
                    }
                    catch (Exception)
                    {
                        vatAmt = "0";
                        vatAmount = 0;
                    }

                    if (vatAmount > 0 && taxPercentage == 0 && invoiceAmount!=0)
                    {
                        decimal tempPer = vatAmount * 100;
                        taxPercentage = tempPer / invoiceAmount;
                        taxPercentage = Math.Round(taxPercentage, 2);
                    }
                }

                if (tableName == "Invoice")
                {
                    decimal nonVatableAmount = 0;
                    decimal vatableAmount = 0;
                    decimal priorityFee = 0;

                    dtExtractedValues.AcceptChanges();

                    XDocument doc = XDocument.Load(filePath);
                    int count = 0;

                    if (dtExtractedValues.Columns.Contains("_NonVatableAmount") && dtExtractedValues.Rows[0]["_NonVatableAmount"].ToString() != "" && dtExtractedValues.Rows[0]["_VatableNet"].ToString() != "" && dtExtractedValues.Rows[0]["_PriorityFee"].ToString() != "")
                    {
                        if (dtExtractedValues.Rows[0]["_VatableNet"].ToString().Contains("."))
                        {
                            string nonVatAmt = dtExtractedValues.Rows[0]["_NonVatableAmount"].ToString().Trim();
                            if (nonVatAmt.Contains("£"))
                            {
                                nonVatAmt = nonVatAmt.Replace("£", "");
                            }
                            else if (nonVatAmt.Contains("$"))
                            {
                                nonVatAmt = nonVatAmt.Replace("$", "");
                            }
                            else if (nonVatAmt.Contains("€"))
                            {
                                nonVatAmt = nonVatAmt.Replace("€", "");
                            }

                            try
                            {
                                IsAddissionLee = true;
                                nonVatableAmount = Convert.ToDecimal(nonVatAmt);
                                invoiceAmount = invoiceAmount + nonVatableAmount - vatAmount;
                            }
                            catch (Exception)
                            {
                            }
                        }
                    }

                    foreach (XElement record in doc.Descendants("_Table"))
                    {
                        string parkingRef = "";
                        string fractionalValues = "";
                        decimal parkAmt = 0;

                        DataRow row = dtExtractedValues.NewRow();

                        row["_InvoiceDate"] = dtExtractedValues.Rows[0]["_InvoiceDate"].ToString();
                        row["_InvoiceNumber"] = dtExtractedValues.Rows[0]["_InvoiceNumber"].ToString();
                        row["_InvoiceAmount"] = invoiceAmount;
                        row["_VATAmount"] = vatAmount;
                        row["_VATNo"] = dtExtractedValues.Rows[0]["_VATNo"].ToString();
                        row["_Currency"] = dtExtractedValues.Rows[0]["_Currency"].ToString();
                        row["_Currency2"] = dtExtractedValues.Rows[0]["_Currency2"].ToString();

                        if (record.Element("_Date").Value.ToString() != "")
                        {
                            row["_DateNew"] = record.Element("_Date").Value;
                        }
                        else
                        {
                            row["_DateNew"] = "";
                        }

                        if (record.Element("_Time").Value.ToString() != "")
                        {
                            row["_Time"] = record.Element("_Time").Value;
                        }
                        else
                        {
                            row["_Time"] = "";
                        }

                        if (record.Element("_Ourref").Value.ToString() != "")
                        {
                            fractionalValues = record.Element("_Ourref").Value.ToString().Trim();
                            //fractionalValues = getNumber(fractionalValues);
                            row["_Ourref"] = fractionalValues;
                        }
                        else
                        {
                            row["_Ourref"] = "";
                        }

                        if (record.Element("_YourReference").Value.ToString() != "")
                        {
                            row["_YourReference"] = record.Element("_YourReference").Value;
                            parkingRef = row["_YourReference"].ToString().Trim();
                        }
                        else
                        {
                            row["_YourReference"] = "";
                        }

                        if (record.Element("_Mins").Value.ToString() != "")
                        {
                            row["_Mins"] = record.Element("_Mins").Value;
                            if (IsLetterContain(row["_Mins"].ToString().Trim()))
                            {
                                row["_Mins"] = "0";
                            }
                        }
                        else
                        {
                            row["_Mins"] = "0";
                        }

                        if (record.Element("_Wait").Value.ToString() != "")
                        {
                            row["_Wait"] = record.Element("_Wait").Value;
                        }
                        else
                        {
                            row["_Wait"] = "0";
                        }

                        if (record.Element("_Misc").Value.ToString() != "")
                        {
                            string priceValue = record.Element("_Misc").Value;

                            int indexOfDot = priceValue.ToString().IndexOf(".");
                            priceValue = priceValue.Substring(0, indexOfDot + 3);
                            row["_Misc"] = priceValue;
                        }
                        else
                        {
                            row["_Misc"] = "0";
                        }

                        if (record.Element("_Price").Value.ToString() != "")
                        {
                            string priceValue = record.Element("_Price").Value;

                            int indexOfDot = priceValue.ToString().IndexOf(".");
                            priceValue = priceValue.Substring(0, indexOfDot + 3);

                            if (IsLetterContain(priceValue))
                            {
                                priceValue = "0";
                            }
                            else
                            {
                                row["_Price"] = priceValue;
                            }


                            if (Convert.ToDecimal(row["_Misc"]) == 0 && Convert.ToDecimal(priceValue) != 0)
                            {
                                row["_Misc"] = priceValue;
                            }
                        }
                        else
                        {
                            row["_Price"] = "0";
                        }

                        if (record.Element("_Vat").Value.ToString() != "")
                        {
                            string tempVATCode = record.Element("_Vat").Value.ToString().Trim();
                            decimal vatCalc = 0;
                            if (tempVATCode.Contains("20"))
                            {
                                vatCalc = Convert.ToDecimal(row["_Misc"]) * Convert.ToDecimal(0.2);
                                row["_Vat"] = vatCalc;
                            }
                            else
                            {
                                row["_Vat"] = "0";
                            }
                        }
                        else
                        {
                            row["_Vat"] = "0";
                        }

                        if (record.Element("_Total").Value.ToString() != "")
                        {
                            string priceValue = record.Element("_Total").Value;

                            int indexOfDot = priceValue.ToString().IndexOf(".");
                            if (indexOfDot != -1)
                            {
                                priceValue = priceValue.Substring(0, indexOfDot + 3);
                                row["_Total"] = priceValue;

                                //if (IsAddissionLee)
                                //{
                                //    parkAmt = Convert.ToDecimal(priceValue);
                                //    parkAmt = parkAmt + (Convert.ToDecimal(0.1) * parkAmt);
                                //}

                                parkAmt = Convert.ToDecimal(priceValue);
                            }
                        }
                        else
                        {
                            row["_Total"] = "0";
                        }

                        if (record.Element("_Service").Value.ToString() != "")
                        {
                            row["_Service"] = record.Element("_Service").Value;
                        }
                        else if (parkingRef != "Parking Fee")
                        {
                            row["_Service"] = "";
                            continue;
                        }

                        if (parkingRef != "Parking Fee")
                        {

                            if (IsAddissionLee)
                            {
                                decimal prevTotalAmt = Convert.ToDecimal(row["_Total"]);
                                prevTotalAmt = prevTotalAmt + (Convert.ToDecimal(0.1) * prevTotalAmt);
                                decimal vatAmt = 0;
                                if (record.Element("_Vat").Value.ToString() != "")
                                {
                                    string tempVAT = record.Element("_Vat").Value.ToString().Trim();
                                    if (tempVAT == "01")
                                    {
                                        vatAmt =  (Convert.ToDecimal(0.2) * prevTotalAmt);

                                    }
                                }
                                prevTotalAmt = Math.Round(prevTotalAmt, 2);
                                vatAmt = Math.Round(vatAmt, 2);
                                row["_Vat"] = vatAmt;
                                row["_Total"] = prevTotalAmt;
                                
                            }

                            dtExtractedValues.Rows.Add(row);
                            dtExtractedValues.AcceptChanges();
                            count++;
                        }
                        else
                        {
                            decimal prevTotalAmt = Convert.ToDecimal(dtExtractedValues.Rows[count]["_Total"]);
                            decimal prevMisc = Convert.ToDecimal(dtExtractedValues.Rows[count]["_Misc"]);
                            decimal vatAmt = 0;


                            parkAmt = parkAmt + (Convert.ToDecimal(0.1) * parkAmt);

                            if (record.Element("_Vat").Value.ToString() != "")
                            {
                                string tempVAT = record.Element("_Vat").Value.ToString().Trim();
                                if (tempVAT == "01")
                                {
                                    vatAmt =  (Convert.ToDecimal(0.2) * (prevTotalAmt + parkAmt));
                                }
                            }

                            row["_Vat"] = vatAmt;
                            prevTotalAmt = prevTotalAmt + parkAmt;
                            prevTotalAmt = Math.Round(prevTotalAmt, 2);
                            dtExtractedValues.Rows[count]["_Total"] = prevTotalAmt;
                            dtExtractedValues.Rows[count]["_Misc"] = prevMisc + parkAmt;
                            dtExtractedValues.AcceptChanges();
                            continue;
                        }
                    }

                }
                else if (tableName == "Invoices2")
                {
                    dtExtractedValues.Rows[0]["_InvoiceAmount"] = invoiceAmount;
                    dtExtractedValues.Rows[0]["_VATAmount"] = vatAmount;
                    bool isDateFail = false;
                    if (dtExtractedValues.Rows[0]["_InvoiceDate"].ToString().Trim() != "")
                    {
                        try
                        {
                            DateTime tempDate = Convert.ToDateTime(dtExtractedValues.Rows[0]["_InvoiceDate"].ToString().Trim());
                            dtExtractedValues.Rows[0]["_InvoiceDate"] = tempDate.ToShortDateString();
                        }
                        catch (Exception)
                        {
                            isDateFail = true;
                            dtExtractedValues.Rows[0]["_InvoiceDate"] = DateTime.Now.ToShortDateString();
                        }
                    }
                    else
                    {
                        isDateFail = true;
                    }

                    if (isDateFail && dtExtractedValues.Rows[0]["_SecondaryDate"].ToString().Trim() != "")
                    {
                        string tempDate = dtExtractedValues.Rows[0]["_SecondaryDate"].ToString().Trim();
                        tempDate = tempDate.ToLower();
                        if (tempDate.Contains("st"))
                        {
                            tempDate = tempDate.Replace("st", "");
                        }
                        else if (tempDate.Contains("th"))
                        {
                            tempDate = tempDate.Replace("th", "");
                        }

                        if (tempDate.Contains("o"))
                        {
                            tempDate = tempDate.Replace("o", "0");
                        }

                        if (tempDate.Contains("l"))
                        {
                            tempDate = tempDate.Replace("l", "1 ");
                        }

                        try
                        {
                            dtExtractedValues.Rows[0]["_InvoiceDate"] = Convert.ToDateTime(tempDate).ToShortDateString();
                        }
                        catch (Exception)
                        {
                            dtExtractedValues.Rows[0]["_InvoiceDate"] = DateTime.Now.ToShortDateString();
                        }

                    }
                    else
                    {
                        if (dtExtractedValues.Rows[0]["_InvoiceDate"].ToString().Trim() == "")
                        {
                            dtExtractedValues.Rows[0]["_InvoiceDate"] = DateTime.Now.ToShortDateString();
                        }
                    }

                    dtExtractedValues.Rows[0]["_DateNew"] = dtExtractedValues.Rows[0]["_InvoiceDate"];
                    dtExtractedValues.Rows[0]["_Time"] = 0;
                    dtExtractedValues.Rows[0]["_Service"] = docName;
                    dtExtractedValues.Rows[0]["_Ourref"] = docName;
                    dtExtractedValues.Rows[0]["_YourReference"] = docName;
                    dtExtractedValues.Rows[0]["_Price"] = invoiceAmount;
                    dtExtractedValues.Rows[0]["_Mins"] = 0;
                    dtExtractedValues.Rows[0]["_Wait"] = 0;
                    dtExtractedValues.Rows[0]["_Misc"] = 0;
                    dtExtractedValues.Rows[0]["_Vat"] = vatAmount;
                    dtExtractedValues.Rows[0]["_Total"] = 0;
                }
                else if (tableName == "UniversalInvoices")
                {
                    XDocument doc = XDocument.Load(filePath);
                    if (doc.Descendants("_Table") != null)
                    {
                        DataRow row = dtExtractedValues.NewRow();
                        foreach (XElement record in doc.Descendants("_Table"))
                        {
                            row = dtExtractedValues.NewRow();
                            string amount = "0";
                            string vat = "0";

                            row["_InvoiceDate"] = dtExtractedValues.Rows[0]["_InvoiceDate"].ToString();
                            row["_InvoiceNumber"] = dtExtractedValues.Rows[0]["_InvoiceNumber"].ToString();
                            row["_InvoiceAmount"] = invoiceAmount;
                            row["_VATAmount"] = vatAmount;
                            row["_VATNo"] = dtExtractedValues.Rows[0]["_VATNo"].ToString();
                            row["_Currency"] = dtExtractedValues.Rows[0]["_Currency"].ToString();
                            row["_Currency2"] = dtExtractedValues.Rows[0]["_Currency2"].ToString();

                            row["_Ourref"] = record.Element("_Description").Value.ToString().Trim();

                            if (record.Element("_Amount").Value.ToString() != "")
                            {

                                //Saving Amount
                                amount = record.Element("_Amount").Value.ToString().ToString().Trim();
                                if (amount.Contains("£"))
                                {
                                    amount = amount.Replace("£", "");
                                }
                                else if (amount.Contains("$"))
                                {
                                    amount = amount.Replace("$", "");
                                }
                                else if (amount.Contains("€"))
                                {
                                    amount = amount.Replace("€", "");
                                }

                                try
                                {
                                    row["_Total"] = Convert.ToDecimal(amount);
                                }
                                catch (Exception)
                                {
                                    row["_Total"] = 0.00m;
                                }

                                //Saving VAT
                                if (record.Element("_VAT").Value.ToString() != "")
                                {
                                    vat = record.Element("_VAT").Value.ToString().ToString().Trim();
                                    if (vat.Contains("£"))
                                    {
                                        vat = vat.Replace("£", "");
                                    }
                                    else if (vat.Contains("$"))
                                    {
                                        vat = vat.Replace("$", "");
                                    }
                                    else if (vat.Contains("€"))
                                    {
                                        vat = vat.Replace("€", "");
                                    }
                                    try
                                    {
                                        row["_Vat"] = Convert.ToDecimal(vat);
                                    }
                                    catch (Exception)
                                    {
                                        row["_Vat"] = 0.00m;
                                    }
                                }
                            }
                            else if (record.Element("_VAT").Value.ToString() != "")
                            {
                                amount = record.Element("_VAT").Value.ToString().ToString().Trim();
                                if (amount.Contains("£"))
                                {
                                    amount = amount.Replace("£", "");
                                }
                                else if (amount.Contains("$"))
                                {
                                    amount = amount.Replace("$", "");
                                }
                                else if (amount.Contains("€"))
                                {
                                    amount = amount.Replace("€", "");
                                }
                                try
                                {
                                    row["_Total"] = Convert.ToDecimal(amount);
                                }
                                catch (Exception)
                                {
                                    row["_Total"] = 0.00m;
                                }
                                row["_VAT"] = 0.00m;
                            }
                            else
                            {
                                row["_Total"] = 0.00m;
                                row["_VAT"] = 0.00m;
                            }

                            if (record.Element("_Quantity").Value.ToString() != "")
                            {
                                string qty = record.Element("_Quantity").Value.ToString();
                                try
                                {
                                    row["_Mins"] = Convert.ToInt32(qty);
                                }
                                catch (Exception)
                                {
                                    row["_Mins"] = 0;
                                }
                            }

                            if (record.Element("_UnitPrice").Value.ToString() != "")
                            {
                                string qty = record.Element("_UnitPrice").Value.ToString();
                                try
                                {
                                    row["_Price"] = Convert.ToDecimal(qty);
                                }
                                catch (Exception)
                                {
                                    row["_Price"] = 0;
                                }
                            }
                            dtExtractedValues.Rows.Add(row);
                            dtExtractedValues.AcceptChanges();
                        }
                    }
                }


                if(CurSysmbol=="")
                {
                    if (dtExtractedValues.Columns.Contains("_Currency2") && dtExtractedValues.Rows[0]["_Currency2"].ToString() != "")
                    {
                        CurSysmbol = dtExtractedValues.Rows[0]["_Currency2"].ToString();
                    }
                }
                if (dtExtractedValues.Rows.Count > 1)
                {
                    dtExtractedValues.Rows[0].Delete();
                }
                else
                {
                    if (tableName == "Invoice")
                    {
                        dtExtractedValues.Rows[0]["_DateNew"] = DateTime.Now;
                        dtExtractedValues.Rows[0]["_Time"] = 0;
                        dtExtractedValues.Rows[0]["_Service"] = docName;
                        dtExtractedValues.Rows[0]["_Ourref"] = 0;
                        dtExtractedValues.Rows[0]["_YourReference"] = 0;
                        dtExtractedValues.Rows[0]["_Price"] = 0;
                        dtExtractedValues.Rows[0]["_Mins"] = 0;
                        dtExtractedValues.Rows[0]["_Wait"] = 0;
                        dtExtractedValues.Rows[0]["_Misc"] = 0;
                        dtExtractedValues.Rows[0]["_Vat"] = 0;
                        dtExtractedValues.Rows[0]["_Total"] = 0;
                    }
                }
                dtExtractedValues.AcceptChanges();



                DocumentMaster objMaster = new DocumentMaster();
                objMaster.DocumentUniuqId = fileUniqueId;
                if (dtExtractedValues.Columns.Contains("_InvoiceNo"))
                {
                    objMaster.DocumentNo = dtExtractedValues.Rows[0]["_InvoiceNo"].ToString().Trim();
                }
                
                //objMaster.DocumentNo = dtExtractedValues.Rows[0]["_DocumentNo"].ToString().Trim();
                //documentDataJSON.documentDetails.documentMaster.DocumentUniuqId = fileUniqueId;
                //documentDataJSON.documentDetails.documentMaster.DocumentNo = dtExtractedValues.Rows[0]["_InvoiceNumber"].ToString().Trim();
                if (dtExtractedValues.Rows[0]["_Currency"].ToString() != "")
                {
                    string curCode = dtExtractedValues.Rows[0]["_Currency"].ToString().Trim();

                    if (curCode.Contains("£") || CurSysmbol== "£")
                    {
                        curCode = "GBP";
                    }
                    else if (curCode.Contains("$") || CurSysmbol == "$")
                    {
                        curCode = "USD";
                    }
                    else if (curCode.Contains("€") || CurSysmbol == "€")
                    {
                        curCode = "EUR";
                    }
                    else
                    {
                        if (dtExtractedValues.Columns.Contains("_Currency2"))
                        {
                            curCode = dtExtractedValues.Rows[0]["_Currency2"].ToString();
                        }
                        
                        if (curCode.Contains("GBP") ||  CurSysmbol == "GBP")
                        {
                            curCode = "GBP";
                        }
                        else if (curCode.Contains("USD") || CurSysmbol == "USD")
                        {
                            curCode = "USD";
                        }
                        else if (curCode.Contains("EUR") || CurSysmbol == "EUR")
                        {
                            curCode = "EUR";
                        }
                    }
                    //documentDataJSON.documentDetails.documentMaster.DocumentCurrency = curCode;                    
                    
                    objMaster.DocumentCurrency = curCode;
                    
                    
                }
                else
                {
                    //documentDataJSON.documentDetails.documentMaster.DocumentCurrency = dtExtractedValues.Rows[0]["_Currency2"].ToString();
                    if(CurSysmbol!="")
                    {
                        objMaster.DocumentCurrency = CurSysmbol;
                    }
                    else
                    {
                        if (dtExtractedValues.Columns.Contains("_Currency2"))
                        {
                            objMaster.DocumentCurrency = dtExtractedValues.Rows[0]["_Currency2"].ToString();
                        }
                        
                    }
                    
                }

                //documentDataJSON.documentDetails.documentMaster.DocumentDate = dtExtractedValues.Rows[0]["_InvoiceDate"].ToString();
                if (dtExtractedValues.Rows[0]["_InvoiceDate"].ToString() != "")
                {
                    try
                    {
                        string date2= Convert.ToDateTime(dtExtractedValues.Rows[0]["_InvoiceDate"]).ToString();
                        string[] date3 = date2.Split(' ');
                        objMaster.DocumentDate = date3[0];
                    }
                    catch(Exception)
                    {
                        try
                        {
                            string tempDate = dtExtractedValues.Rows[0]["_InvoiceDate"].ToString();
                        string[] temp2 = tempDate.Split(' ');
                        string intDay = "";
                        if (temp2[0] == "1st" || temp2[0] == "01") { intDay = "01"; }
                        else if (temp2[0] == "2nd" || temp2[0] == "02" || temp2[0] == "2") { intDay = "02"; }
                        else if (temp2[0] == "3rd" || temp2[0] == "03" || temp2[0] == "3") { intDay = "03"; }
                        else if (temp2[0] == "4th" || temp2[0] == "04" || temp2[0] == "4") { intDay = "04"; }
                        else if (temp2[0] == "5th" || temp2[0] == "05" || temp2[0] == "5") { intDay = "05"; }
                        else if (temp2[0] == "6th" || temp2[0] == "06" || temp2[0] == "6") { intDay = "06"; }
                        else if (temp2[0] == "7th" || temp2[0] == "07" || temp2[0] == "7") { intDay = "07"; }
                        else if (temp2[0] == "8th" || temp2[0] == "08" || temp2[0] == "8") { intDay = "08"; }
                        else if (temp2[0] == "9th" || temp2[0] == "09" || temp2[0] == "9") { intDay = "09"; }
                        else if (temp2[0] == "10th" || temp2[0] == "10") { intDay = "10"; }
                        else if (temp2[0] == "11th" || temp2[0] == "11") { intDay = "11"; }
                        else if (temp2[0] == "12th" || temp2[0] == "12") { intDay = "12"; }
                        else if (temp2[0] == "13th" || temp2[0] == "13") { intDay = "13"; }
                        else if (temp2[0] == "14th" || temp2[0] == "14") { intDay = "14"; }
                        else if (temp2[0] == "15th" || temp2[0] == "15") { intDay = "15"; }
                        else if (temp2[0] == "16th" || temp2[0] == "16") { intDay = "16"; }
                        else if (temp2[0] == "17th" || temp2[0] == "17") { intDay = "17"; }
                        else if (temp2[0] == "18th" || temp2[0] == "18") { intDay = "18"; }
                        else if (temp2[0] == "19th" || temp2[0] == "19") { intDay = "19"; }
                        else if (temp2[0] == "20th" || temp2[0] == "20") { intDay = "20"; }
                        else if (temp2[0] == "21st" || temp2[0] == "21") { intDay = "21"; }
                        else if (temp2[0] == "22nd" || temp2[0] == "22") { intDay = "22"; }
                        else if (temp2[0] == "23rd" || temp2[0] == "23") { intDay = "23"; }
                        else if (temp2[0] == "24th" || temp2[0] == "24") { intDay = "24"; }
                        else if (temp2[0] == "25th" || temp2[0] == "25") { intDay = "25"; }
                        else if (temp2[0] == "26th" || temp2[0] == "26") { intDay = "26"; }
                        else if (temp2[0] == "27th" || temp2[0] == "27") { intDay = "27"; }
                        else if (temp2[0] == "28th" || temp2[0] == "28") { intDay = "28"; }
                        else if (temp2[0] == "29th" || temp2[0] == "29") { intDay = "29"; }
                        else if (temp2[0] == "30th" || temp2[0] == "30") { intDay = "30"; }
                        else if (temp2[0] == "31st" || temp2[0] == "31") { intDay = "31"; }
                        string intmonth = "";
                        #region month
                        if (temp2[1]== "January" || temp2[1] == "Jan") { intmonth = "01"; }
                        else if (temp2[1] == "February" || temp2[1] == "Feb") { intmonth = "02"; }
                        else if (temp2[1] == "March" || temp2[1] == "Mar") { intmonth = "03"; }
                        else if (temp2[1] == "April" || temp2[1] == "Apr") { intmonth = "04"; }
                        else if (temp2[1] == "May") { intmonth = "05"; }
                        else if (temp2[1] == "June" || temp2[1] == "Jun") { intmonth = "06"; }
                        else if (temp2[1] == "July" || temp2[1] == "Jul") { intmonth = "07"; }
                        else if (temp2[1] == "August" || temp2[1] == " Aug") { intmonth = "08"; }
                        else if (temp2[1] == "September" || temp2[1] == "Sep" || temp2[1] == "Sept") { intmonth = "09"; }
                        else if (temp2[1] == "October" || temp2[1] == " Oct") { intmonth = "10"; }
                        else if (temp2[1] == "November" || temp2[1] == "Nov") { intmonth ="11"; }
                        else if (temp2[1] == "December" || temp2[1] == "Dec") { intmonth = "12"; }
                        #endregion 
                        string intYear = temp2[2].ToString();
                        string tempdate2=""+intDay+"/"+intmonth + "/" + intYear+"";
                        
                             string intdate2= Convert.ToDateTime(tempdate2).ToString();
                            string[] intdate3 = intdate2.Split(' ');
                            objMaster.DocumentDate = intdate3[0].ToString();
                        }
                        catch(Exception)
                        {
                            objMaster.DocumentDate = "";
                        }
                    }

                }
                else
                {
                    objMaster.DocumentDate = DateTime.Now.ToString("dd/MM/yyyy");
                }

                //documentDataJSON.documentDetails.documentMaster.Amount = Convert.ToDecimal(dtExtractedValues.Rows[0]["_InvoiceAmount"].ToString());
                if (dtExtractedValues.Rows[0]["_InvoiceTotal"].ToString() != "")
                {
                    string invAmt = dtExtractedValues.Rows[0]["_InvoiceTotal"].ToString().Trim();

                    int dotCount = invAmt.Count(x => x == '.');
                    if (dotCount > 1)
                    {
                        int indexOfDot = invAmt.LastIndexOf(".");
                        invAmt = invAmt.Remove(indexOfDot, 1);
                    }

                    try
                    {
                        if (invAmt.Contains("£"))
                        {
                            invAmt = invAmt.Replace("£", "");
                            CurSysmbol = "£";
                        }
                        else if (invAmt.Contains("$"))
                        {
                            invAmt = invAmt.Replace("$", "");
                            CurSysmbol = "$";
                        }
                        else if (invAmt.Contains("€"))
                        {
                            invAmt = invAmt.Replace("€", "");
                            CurSysmbol = "€";
                        }

                        if (invAmt.Contains("GBP"))
                        {
                            invAmt = invAmt.Replace("GBP", "");
                            CurSysmbol = "GBP";
                        }
                        if (invAmt.Contains("USD"))
                        {
                            invAmt = invAmt.Replace("USD", "");
                            CurSysmbol = "USD";
                        }
                        if (invAmt.Contains("EUR"))
                        {
                            invAmt = invAmt.Replace("EUR", "");
                            CurSysmbol = "EUR";
                        }

                        if (invAmt.Contains(","))
                        {
                            invAmt = invAmt.Replace(",", "");
                        }


                        objMaster.Amount = Convert.ToDecimal(invAmt);                        
                    }
                    catch (Exception)
                    {
                        invAmt = "0";
                        objMaster.Amount = 0;
                    }
                    //objMaster.Amount = Convert.ToDecimal(dtExtractedValues.Rows[0]["_InvoiceAmount"].ToString());
                }
                else
                {
                    objMaster.Amount = 0;
                }

                //documentDataJSON.documentDetails.documentMaster.VAT = Convert.ToDecimal(dtExtractedValues.Rows[0]["_VATAmount"].ToString());
                if (dtExtractedValues.Rows[0]["_VATTotal"].ToString() != "")
                {
                    string vatAmt = dtExtractedValues.Rows[0]["_VATTotal"].ToString().Trim();//_VATAmount
                    if (vatAmt.Contains("£"))
                    {
                        vatAmt = vatAmt.Replace("£", "");
                    }
                    else if (vatAmt.Contains("$"))
                    {
                        vatAmt = vatAmt.Replace("$", "");
                    }
                    else if (vatAmt.Contains("€"))
                    {
                        vatAmt = vatAmt.Replace("€", "");
                    }

                    if (vatAmt.Contains("GBP"))
                    {
                        vatAmt = vatAmt.Replace("GBP", "");
                    }
                    if (vatAmt.Contains("USD"))
                    {
                        vatAmt = vatAmt.Replace("USD", "");
                    }
                    if (vatAmt.Contains("EUR"))
                    {
                        vatAmt = vatAmt.Replace("EUR", "");
                    }

                    if (vatAmt.Contains(","))
                    {
                        vatAmt = vatAmt.Replace(",", "");
                    }

                    try
                    {
                        objMaster.VAT = Convert.ToDecimal(vatAmt);

                        if (vatAmount > invoiceAmount)
                        {
                            vatAmount = vatAmount - invoiceAmount;
                        }
                        else if (vatAmount == invoiceAmount)
                        {
                            vatAmount = 0;
                        }
                    }
                    catch (Exception)
                    {
                        vatAmt = "0";
                        objMaster.VAT = 0;
                    }
                    //objMaster.VAT = Convert.ToDecimal(dtExtractedValues.Rows[0]["_VATAmount"].ToString());
                }
                else
                {
                    objMaster.VAT = 0;
                }

                //documentDataJSON.documentDetails.documentMaster.VATNo = dtExtractedValues.Rows[0]["_VATNo"].ToString();
                if (dtExtractedValues.Columns.Contains("_VatNo"))
                {
                    string finalVRN = getVATNumber(dtExtractedValues.Rows[0]["_VatNo"].ToString().Trim());
                    objMaster.VATNo = finalVRN;
                }
                else
                {
                    objMaster.VATNo = "";
                }
                

                //download URL is created in the properies and declared in Web.Config file
                //documentDataJSON.documentDetails.documentMaster.DownloadURL = fileUniqueId;
                objMaster.DownloadURL = fileUniqueId;

                DocumentDetails objDocDetails = new DocumentDetails();
                objDocDetails.documentMaster = objMaster;


                List<DocumentTable> lstDocumentTable = new List<DocumentTable>();
                
                if(TableNameNew=="Table1")
                {
                    if (dtTable.Rows.Count > 0)
                    {

                        foreach (DataRow dr in dtTable.Rows)
                        {
                            if (dtTable.Columns.Contains("_Desscription"))
                            {
                                dr["_Ourref"] = dr["_Desscription"].ToString();
                            }
                            if (dtTable.Columns.Contains("_Quantity"))
                            {
                                dr["_Mins"] = dr["_Quantity"].ToString();
                            }
                        }                      
                    }
                    
                }
                else if(TableNameNew == "Table")
                {

                    if (dtTable.Rows.Count > 0)
                    {

                        foreach (DataRow dr in dtTable.Rows)
                        {
                            if (dtTable.Columns.Contains("_Description"))
                            {
                                dr["_Ourref"] = dr["_Description"].ToString();
                            }
                            if (dtTable.Columns.Contains("_Qty"))
                            {
                                dr["_Mins"] = dr["_Qty"].ToString();
                            }
                            if (dtTable.Columns.Contains("_Amount"))
                            {
                                string[] tempAmt= dr["_Amount"].ToString().Split('.');
                                dr["_Total"] = tempAmt[0].ToString();
                            }
                            if (dtTable.Columns.Contains("_VAT"))
                            {
                                dr["_Vat"] = dr["_VAT"].ToString();
                            }                            
                            if (dtTable.Columns.Contains("_rate"))
                            {
                                dr["_Price"] = dr["_rate"].ToString();
                            }

                        }
                    }
                }
                if (dtTable.Rows.Count > 0)
                {

                    foreach (DataRow item in dtTable.Rows)
                    {
                        lstDocumentTable.Add(new DocumentTable
                        {
                            Description = item["_Ourref"].ToString().Trim(),
                            Date = item["_InvoiceDate"].ToString().Trim(),
                            Amount = item["_Total"].ToString().Trim(),
                            VAT = item["_Vat"].ToString().Trim(),
                            Quantity = item["_Mins"].ToString().Trim(),
                            UnitPrice = item["_Price"].ToString().Trim()
                        });
                    }
                }

                objDocDetails.documentTables = lstDocumentTable;

                documentDataJSON.documentDetails = objDocDetails;

                return documentDataJSON;
            }
            catch (Exception ex)
            {
                LogError.TraceException(ex, "-");
                return null;
            }
        }

        public string getNumber(string inputValue)
        {
            inputValue = Regex.Match(inputValue, @"\d+").Value;
            return inputValue;
        }

        public string getVATNumber(string inputValue)
        {
            inputValue = inputValue.Replace(" ", "");
            inputValue = Regex.Match(inputValue, @"\d+").Value;
            return inputValue;
        }

        //Checked if string contains letters or not 
        public bool IsLetterContain(string inputValue)
        {
            int count = Regex.Matches(inputValue, @"[a-zA-Z]").Count;
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //Upload file to Scan Engine Server and FTP server for download link
        private void UploadFile(string fileUniqueId, string filePath)
        {

            //Upload file to Scan Engine server for exproting data to XML
            //FtpWebRequest request =(FtpWebRequest)WebRequest.Create("ftp://Administrator@expensedoc.com:5001/Import/" + fileUniqueId + ".pdf");
            //using (System.Net.WebClient client = new System.Net.WebClient()) //System.Net.WebClient client = new System.Net.WebClient()
            //{
            //    client.Credentials = new System.Net.NetworkCredential("Administrator", "J@Y47+4**sT2p");
            //    client.Proxy = new WebProxy();                
            //    FileInfo fi = new FileInfo(filePath);
            //    client.UploadFile("ftp://Administrator@ma.expensedoc.com" + "//" + fi.Name, "STOR", filePath);
            //}





            string temp = "ftp://Administrator@ma.expensedoc.com:21/Hotfolder/Import/" + fileUniqueId + ".pdf";
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(temp);
            //request.Credentials = new NetworkCredential("Administrator", "J@Y47+4**sT2p");
            request.Credentials = new NetworkCredential("Administrator", "XtSys32Est10");
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.UsePassive = true;
            request.EnableSsl = true;

            using (Stream fileStream = File.OpenRead(filePath))
            using (Stream ftpStream = request.GetRequestStream())
            {
                fileStream.CopyTo(ftpStream);
            }


            //Upload file to systematics server for creating download link
            //request =(FtpWebRequest)WebRequest.Create("ftp://u37623104-download@u37623104.1and1-data.host/ExpenseDoc%20Storage/" + fileUniqueId + ".pdf");
            //request.Credentials = new NetworkCredential("u37623104-download", "XtSys32Est10");
            //request =(FtpWebRequest)WebRequest.Create("ftp://home119338163.1and1-data.host/ExpenseDoc%20Storage/" + fileUniqueId + ".pdf");
            //request.Credentials = new NetworkCredential("u37623104-jl", "Flamingo123456!");
            //request.Method = WebRequestMethods.Ftp.UploadFile;

            //using (Stream fileStream = File.OpenRead(filePath))
            //using (Stream ftpStream = request.GetRequestStream())
            //{
            //    fileStream.CopyTo(ftpStream);
            //}
            //above is old code for ftp below is new one for sftp
                                          
                // Upload File
                using (SftpClient sftp = new SftpClient("home119338163.1and1-data.host", 22, "u37623104-jl", "Flamingo123456!"))
                {

                    sftp.Connect();
                    sftp.ChangeDirectory("/download/ExpenseDoc Storage");///ExpenseDocStorage
                    using (var uplfileStream = System.IO.File.OpenRead(filePath))
                    {
                        sftp.UploadFile(uplfileStream, Path.GetFileName(filePath), true);
                    }
                    sftp.Disconnect();
                }
            
        }

        //Download exported file from Scan Engine Server
        private bool DownloadDataFile(string fileUniqueId, string filePath)
        {
            try
            {
                //FtpWebRequest request =(FtpWebRequest)WebRequest.Create("ftp://Administrator@expensedoc.com:5001/Export/" + fileUniqueId + ".xml");
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://Administrator@ma.expensedoc.com:21/Hotfolder/Export/" + fileUniqueId + ".xml");
                //request.Credentials = new NetworkCredential("Administrator", "J@Y47+4**sT2p");
                request.Credentials = new NetworkCredential("Administrator", "XtSys32Est10");
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.UsePassive = true;
                request.EnableSsl = true;

                using (Stream ftpStream = request.GetResponse().GetResponseStream())
                using (Stream fileStream = File.Create(filePath))
                {
                    ftpStream.CopyTo(fileStream);
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        //public static ConnectionInfo getSftpConnection(string host, string username, int port, string publicKeyPath)
        //{
        //    return new ConnectionInfo(host, port, username, privateKeyObject(username, publicKeyPath));
        //}

        //private static AuthenticationMethod[] privateKeyObject(string username, string publicKeyPath)
        //{
        //    PrivateKeyFile privateKeyFile = new PrivateKeyFile(publicKeyPath);
        //    PrivateKeyAuthenticationMethod privateKeyAuthenticationMethod =
        //       new PrivateKeyAuthenticationMethod(username, privateKeyFile);
        //    return new AuthenticationMethod[]
        //     {
        //privateKeyAuthenticationMethod
        //     };
        //}
    }
}